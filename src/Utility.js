
var Utility = { };
Utility.normalizeLimit = 5000;
Utility.getUnblemishedName = function (varName) {
	var regex = /^(.+?)(<sub>)?[0-9]*(<\/sub>)?$/;
	return regex.exec(varName)[1];
};
var Type = {
	Variable : 1,
	FunTerm : 3,
	AbsTerm : 4,
	AppTerm : 5,
	SentinelTerm : 6,
	LetAbbr : 7,
	StrTerm : 8,
	Int : 9,
	OpP : 10
};

var Semantic = { }
Semantic.contract = [];


function RefCount (num) {
	this.n = num;
}
RefCount.prototype = {
	constructor: RefCount,
	incr: function () {
		++this.n;
	},
	decr: function () {
		--this.n;
	},
	valueOf: function () {
		return this.n;
	}
}


/**
 * A subterm is the superclass of the listed types.
 * @typedef {(FreeVar|BoundVar|FunTerm|AbsTerm|AppTerm|SentinelTerm)} Subterm
 *
 * A registry of free variables.
 * @typedef {Object.<string,FreeVar>} Registry
 */
