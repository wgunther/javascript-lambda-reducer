function NoOp() { }
NoOp.prototype = {
	constructor: NoOp,
/* Subterm */
	type: function () { throw new DeletedFunction(); },
	children: function () { return []; },
	getName: function () { throw new DeletedFunction(); },
	cloneWithSideEffects: function () { throw new DeletedFunction(); },
	replaceChild: function () { throw new DeletedFunction(); },
	hardDestroy: function () { },
	softDestroy: function () { },
	makeJQueryHTMLObject: function () { throw new DeletedFunction(); },
	isRedex: function () { return false; },
	canApply: function () { return false; }
}
