function Sentinel (term) {
	this.depth = 0;
	this.display = null;

	this.child = term;

	term.parent = this;
}


Sentinel.prototype = {
	constructor: Sentinel,
/* Subterm */
	type: function () {
		return Type.Sentinel;
	},
	children: function () {
		return [this.child];
	},
	getName: function () {
		throw new UnexpectedType("Sentinel");
	},
	cloneWithSideEffects: function () {
		return new Sentinel(this.child.cloneWithSideEffects());
	},
	replaceChild: function (child, newChild) {
		if (this.child === child) {
			//child.parent = null;
			this.child = newChild;
			newChild.parent = this;
		} else {
			throw new InvalidArguments("Method not invoked on parent of child");
		}
	},
	hardDestroy: function () {
		this.parent = null;
		this.display = null;
		
		if (this.child) //XXX: for LetAbbr
			this.child.hardDestroy();

		this.child = null;
	},
	softDestroy: function () {
		this.parent = null;
		this.display = null;

		this.child.softDestroy();
	},
	makeJQueryHTMLObject: function () {
		return this.child.makeJQueryHTMLObject();
	},
	isRedex: function () {
		return false;
	},
	canApply: function () {
		return false; // not able to accept input
	}
}




