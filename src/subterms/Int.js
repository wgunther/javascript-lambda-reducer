function Int (value) {
	this.depth = 0;
	this.display = null;

	this.value = value;
}
Int.prototype = {
	constructor: Int,
/* Subterm */
	type: function () {
		return Type.Int;
	},
	children: function () {
		return [];
	},
	getName: function () {
		return this.value.toString();
	},
	cloneWithSideEffects: function () {
		return new Int(this.value);
	},
	replaceChild: function (child, newChild) {
		throw new Error("No Child");
	},
	hardDestroy: function () {
		this.parent = null;
		this.display = null;
	},
	softDestroy: function () {
		this.parent = null;
		this.display = null;
	},
	makeJQueryHTMLObject: function () {
		this.depth = this.parent.depth + 1;

		var $span = $("<span>");
		$span.append(this.value);

		this.display = $span;
		$span.data("term",this);
		return $span;	
	},
	isRedex: function () {
		return false;
	},
	canApply: function () {
		return false;
	}
}
