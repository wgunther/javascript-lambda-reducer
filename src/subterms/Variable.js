function Variable (registry) {
	this.registry = registry;
	registry.register(this);
}
Variable.prototype = {
	constructor: Variable,
/* Subterm */
	type: function () { 
		return Type.Variable;
	},
	children: function () {
		return [];
	},
	getName: function () {
		return this.registry.getName();
	},
	cloneWithSideEffects: function () {
		return new Variable(this.registry);
	},
	replaceChild: function (child, newChild) {
		throw new Error("No Child");
	},
	hardDestroy: function () {
		this.parent = null;
		this.display = null;
		this.registry.unregister(this);
	},
	softDestroy: function () {
		this.parent = null;
		this.display = null;
	},
	makeJQueryHTMLObject: function () {
		var par = this.parent;
		this.depth = par.depth + 1;

		var $span = $("<span>");
		var name = this.getName();
		$span.append(name);

		this.display = $span;
		$span.data("term", this);
		return $span;
	},
	isRedex: function () {
		return false;
	},
	canApply: function (toTerm) {
		return false;
	}
}


